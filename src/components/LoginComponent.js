import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import axios from "axios";

class LoginComponent extends Component {
    constructor(props) {
        super(props);
        this.validateForm = this.validateForm.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.state = { isLoggedIn: false, username: '', email: '', password: '' };

    }
    setEmail(event) {
        this.setState({ email: event.target.value })
    }

    setPassword(event) {
        this.setState({ password: event.target.value })
    }

    setUsername(event) {
        this.setState({ username: event.target.value })
    }

    validateForm() {
        return this.state.email.length > 0 && this.state.password.length > 0 && this.state.username.length > 0;
    }

    handleSubmit(event) {
        // event.preventDefault();
        const user = {
            email: this.state.email,
            password: this.state.password,
            username: this.state.username,
        }
        fetch('http://localhost:5000/api/login/user', {
            method: 'POST',
            headers:{
                'Content-type': 'application/json',
                'Access-Control-Allow-Origin': '*',
                'Referrer-Policy': 'origin-when-cross-origin'
            },
            body: JSON.stringify(user)
        })
    }

    render() {
        console.log('I was triggered during render')
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="email" bsSize="large">
                        <ControlLabel>Email</ControlLabel>
                        <FormControl
                            autoFocus
                            type="email"
                            value={this.state.email}
                            onChange={e => this.setEmail(e)}
                        />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl
                            value={this.state.password}
                            onChange={e => this.setPassword(e)}
                            type="password"
                        />
                    </FormGroup>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl
                            value={this.state.username}
                            onChange={e => this.setUsername(e)}
                            type="text"
                        />
                    </FormGroup>
                    <Button block bsSize="large" disabled={!this.validateForm()} type="submit">
                        Login
          </Button>
                </form>
            </div>
        );
    }
}

export default LoginComponent;

