import React, { Component } from "react";
import { Button, FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import axios from "axios";

class FavoriteComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { isLoggedIn: false, username: '', email: '', password: '', favorite:'', id: 5};

    }

    getRecommend(){
        fetch('/favorite/:id')
        .then(async response => {
            const data = await response.json();

            // check for error response
            if (!response.ok) {
                // get error message from body or default to response statusText
                const error = (data && data.message) || response.statusText;
                return Promise.reject(error);
            }

            this.setState({ recommend: data.recommend })
        })
        .catch(error => {
            this.setState({ errorMessage: error.toString() });
            console.error('There was an error!', error);
        });
    }

}

export default FavoriteComponent;
