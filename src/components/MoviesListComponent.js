import React, { Component } from "react";
import MovieItemComponent from "./MovieItemComponent";
import axios from "axios";

function searchingFor(term) {
  return function (x) {
    return x.name.toLowerCase().includes(term.toLowerCase()) || !term;
  };
}
class MoviesListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listMovies: [
      ],
      listMoviesLiked: [],
      term: "",
    };
    this.searchHandler = this.searchHandler.bind(this);
  }

  searchHandler(event) {
    this.setState({ term: event.target.value });
  }

  onChangeStatus = (id) => {
    console.log("movie id:" + id);
    this.state.listMovies.map((item, index) => {
      if (index === id) {
        item.isLiked = !item.isLiked;
        console.log("status: " + item.isLiked);
        this.state.listMovies[id] = item;
        this.setState({
          listMovies: this.state.listMovies,
        });

        // localStorage.setItem("list", JSON.stringify(this.state.listMovies));
      }
    });
  };

  componentWillMount() {
    fetch("http://localhost:5000/favorites")
      .then(res => {
        return res.json();
      })
      .then((favorites) => {
        this.setState({
          listMovies : favorites.data,
        });
      })
  }

  componentDidCatch() {
    let list = JSON.parse(localStorage.getItem("list"));
    this.setState({
      listMovies: list,
    });
  }

  render() {
    return (
      <div>
        <div className="row col-xs-6 col-sm-6 col-md-6 col-lg-6 mb-2">
          <div className="input-group">
            <input
              name="keyword"
              type="text"
              className="form-control mb-3"
              placeholder="Nhập từ khóa..."
              onChange={this.searchHandler}
            />
          </div>
        </div>
        <div className="row">
          {this.props.statusLiked === true
            ? this.state.listMovies.map((movie, index) => {
                if (movie.isLiked)
                  return (
                    <MovieItemComponent
                      key={index}
                      id={index}
                      name={movie.name}
                      description={movie.description}
                      rateScore={movie.rateScore}
                      actors={movie.actors}
                      releaseDate={movie.releaseDate}
                      imageUrl={movie.imageUrl}
                      isLiked={movie.isLiked}
                      onUpdateStatus={this.onChangeStatus}
                      onChange={this.onChangeStatus}
                    />
                  );
              })
            : this.state.listMovies
                .filter(searchingFor(this.state.term))
                .map((movie, index) => {
                  return (
                    <MovieItemComponent
                      key={index}
                      id={index}
                      name={movie.name}
                      description={movie.description}
                      rateScore={movie.rateScore}
                      actors={movie.actors}
                      releaseDate={movie.releaseDate}
                      imageUrl={movie.imageUrl}
                      isLiked={movie.isLiked}
                      onUpdateStatus={this.onChangeStatus}
                      onChange={this.onChangeStatus}
                    />
                  );
                })}
        </div>
      </div>
    );
  }
}

export default MoviesListComponent;
