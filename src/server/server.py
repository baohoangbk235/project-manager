from flask import Flask, jsonify, request, abort
from flask_cors import CORS, cross_origin

app = Flask(__name__)

CORS(app)

@app.route('/')
def index():
    return "Hello, World!"

@app.route('/favorites', methods=['GET'])
def get_tasks():
    favorites = [{ 'name': "Joker (2019)",
    'description':
      "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society....",
    'rateScore': 4,
    'actors': [],
    'releaseDate': "21 February 2019 (Vietnam)",
    'imageUrl':
      "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    'isLiked': False,}]
    return jsonify({'data': favorites})

@app.route('/api/login/user', methods=['POST'])
@cross_origin(supports_credentials=True)
def handle_login():
    print (request.get_json())
    return "JSON posted"

if __name__ == '__main__':
    app.run(debug=True)