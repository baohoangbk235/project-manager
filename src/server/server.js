const express = require('express')
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = 5000;

// Where we will keep books
let books = [];

app.use(cors());

// Configuring body parser middleware
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/login/user', (req, res) => {
    // We will be coding here
    console.log(req.body)
    res.send(req.body)
});

app.get('/favorites', (req, res) => {
    console.log("hello")
    let favorites = [{ name: "Joker (2019)",
    description:
      "In Gotham City, mentally troubled comedian Arthur Fleck is disregarded and mistreated by society....",
    rateScore: 4,
    actors: [],
    releaseDate: "21 February 2019 (Vietnam)",
    imageUrl:
      "https://m.media-amazon.com/images/M/MV5BNGVjNWI4ZGUtNzE0MS00YTJmLWE0ZDctN2ZiYTk2YmI3NTYyXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_UX182_CR0,0,182,268_AL_.jpg",
    isLiked: false,}]
    res.json({data: favorites});
})

app.listen(port, () => console.log(`Hello world app listening on port ${port}!`));